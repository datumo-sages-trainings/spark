package pl.datumo.sages

import org.joda.time.DateTime
import org.scalatest.{FlatSpec, Matchers}
import pl.datumo.sages.EnricherUtils.{DateFormat,Unknown}

class EnricherUtilsTest extends FlatSpec with Matchers {

  private val DateTimeCase1: Option[DateTime] = Some(DateTime.parse("2019-01-01", DateFormat))
  private val DateTimeCase2: Option[DateTime] = Some(DateTime.parse("2014-07-26", DateFormat))
  private val DateTimeCase3: Option[DateTime] = None
  private val DateTimeCase4: Option[DateTime] = Some(DateTime.parse("2014-11-26", DateFormat))

  "getMillisFromDateTime" should "return millis of given date" in {
    EnricherUtils.getMillisFromDateTime(DateTimeCase1) should be (Some(1546297200000l))
    EnricherUtils.getMillisFromDateTime(DateTimeCase2) should be (Some(1406325600000l))
    EnricherUtils.getMillisFromDateTime(DateTimeCase3) should be (None)
    EnricherUtils.getMillisFromDateTime(DateTimeCase4) should be (Some(1416956400000l))
  }

  "addLeadingZeroIfMissing" should "add leading zero for month or day" in {
    EnricherUtils.addLeadingZeroIfMissing("1") should be ("01")
    EnricherUtils.addLeadingZeroIfMissing("12") should be ("12")
  }

  "getYearAsString" should "return year as string or Unknown (in case of None)" in {
    EnricherUtils.getYearAsString(DateTimeCase1) should be ("2019")
    EnricherUtils.getYearAsString(DateTimeCase2) should be ("2014")
    EnricherUtils.getYearAsString(DateTimeCase3) should be (Unknown)
    EnricherUtils.getYearAsString(DateTimeCase4) should be ("2014")
  }

  "getMonthAsString" should "return month as string (with leading 0) or Unknown (in case of None)" in {
    EnricherUtils.getMonthAsString(DateTimeCase1) should be ("01")
    EnricherUtils.getMonthAsString(DateTimeCase2) should be ("07")
    EnricherUtils.getMonthAsString(DateTimeCase3) should be (Unknown)
    EnricherUtils.getMonthAsString(DateTimeCase4) should be ("11")
  }

  "getDayAsString" should "return day as string (with leading 0) or Unknown (in case of None)" in {
    EnricherUtils.getDayAsString(DateTimeCase1) should be ("01")
    EnricherUtils.getDayAsString(DateTimeCase2) should be ("26")
    EnricherUtils.getDayAsString(DateTimeCase3) should be (Unknown)
    EnricherUtils.getDayAsString(DateTimeCase4) should be ("26")
  }

  "getQuarterAsString" should "return quarter as string (format QX) or Unknown (in case of None)" in {
    EnricherUtils.getQuarterAsString(DateTimeCase1) should be ("Q1")
    EnricherUtils.getQuarterAsString(DateTimeCase2) should be ("Q3")
    EnricherUtils.getQuarterAsString(DateTimeCase3) should be (Unknown)
    EnricherUtils.getQuarterAsString(DateTimeCase4) should be ("Q4")
  }

  "divide" should "return results of divide as Option[Double]" in {
    EnricherUtils.divide(Some(10d), Some(2l)) should be (Some(5d))
    EnricherUtils.divide(Some(10d), Some(0l)) should be (None)
    EnricherUtils.divide(Some(10.25d), Some(5l)) should be (Some(2.05d))
    EnricherUtils.divide(Some(10.25d), Some(-5l)) should be (None)
  }
}
