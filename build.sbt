name := "spark"
version := "1.0"
scalaVersion := "2.11.12"


libraryDependencies ++= Seq(
  "com.typesafe.scala-logging" % "scala-logging_2.11" % "3.9.0",
  "com.databricks" %% "spark-avro" % "4.0.0",
  "org.apache.spark" % "spark-sql_2.11" % "2.2.2",
  "com.google.cloud.bigdataoss" % "gcs-connector" % "1.6.2-hadoop2",
  "com.google.cloud.bigdataoss" % "bigquery-connector" % "0.10.11-hadoop2",
  "com.typesafe" % "config" % "1.3.3",
  "net.ceedubs" %% "ficus" % "1.1.2",
  "joda-time" % "joda-time" % "2.10.1",
  "org.apache.avro" % "avro" % "1.8.2",
  "net.ruippeixotog" %% "scala-scraper" % "2.1.0",
  "org.scalatest" % "scalatest_2.11" % "3.0.7" % "test",
  "pl.datumo" % "spark-bigquery_2.11" % "0.1.1" from "https://storage.googleapis.com/datumo/jars/spark-bigquery/master/spark-bigquery_2.11-0.1.1.jar",
  "com.github.scopt" % "scopt_2.11" % "4.0.0-RC2"
)


sourceGenerators in Compile += (avroScalaGenerate in Compile).taskValue

run in Compile := (fullClasspath in Compile).value

assemblyMergeStrategy in assembly := {
  case PathList("reference.conf") => MergeStrategy.concat
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case _ => MergeStrategy.first
}

assemblyShadeRules in assembly := Seq(
  ShadeRule.rename("com.fasterxml.jackson.core.**" -> "shaded.jackson.core.@1").inAll,
  ShadeRule.rename("com.fasterxml.jackson.annotation.**" -> "shaded.jackson.annotation.@1").inAll,
  ShadeRule.rename("com.fasterxml.jackson.databind.**" -> "shaded.jackson.databind.@1").inAll
)
